$(function () {
    var tagsList = $('.list-inline-item');
    var divTags = $('#tags-choos');
    var input = $('#article_tagsAsString');


    $(document).mouseup(function (e){
        if (!divTags.is(e.target)
            && divTags.has(e.target).length === 0) {
            if (!divTags.hasClass( "close" )) {
                divTags.toggleClass("close");
            }
        }
    });


    tagsList.on('click', function (e) {
        let tag = $(this).html();
        let tagInput = $('#article_tagsAsString');
        let value = tagInput.val();
        if (value === ''){
            tagInput.val(tag);
        } else {
            tagInput.val(value + ', '+ tag);
        }
    });

    input.on('click', function () {
        divTags.toggleClass("close");
    });

    $('#myForm').validate({
        messages: {
            'article[subject]' : '<span class="my-error">ERROR</span> Поле тема обязательное для заполнения',
            'article[date]' : '<span class="my-error">ERROR</span> Поле дата обязательное для заполнения',
            'article[tagsAsString]' : '<span class="my-error">ERROR</span> Поле теги обязательное для заполнения',
            'article[text]' : '<span class="my-error">ERROR</span> Поле текст обязательное для заполнения',
        }
    })
});
<?php

namespace App\DataFixtures;

use App\Entity\Article;
use App\Entity\Tag;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;


class AppFixtures extends Fixture
{
    private $tagsObject = [];
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create();

        $tags = [
            'php', 'java', 'pyton', 'javaScript' , 'programmer' ,
            'job' , 'html' , 'css' , 'news' , 'iPhone' , 'samsung',
            'шаблоны', 'php паттернры','адаптер','ООП','MVS','машинное обучение',
            'symfony','laravel','phalcon',
        ];

        for ($i = 0; $i < count($tags); $i++) {
            $tag = new Tag();
            $tag->setTag($tags[$i]);
            $manager->persist($tag);
            $this->tagsObject[] = $tag;
        }

        for ($i = 0; $i < 15; $i++){
            $article = new Article();
            $article->setSubject($faker->realText(50));
            for ($j = 0; $j < rand(2, 5); $j++){
                $tagRand = array_rand($this->tagsObject);
                if (!$article->getTags()->contains($this->tagsObject[$tagRand])){
                    $article->getTags()->add($this->tagsObject[$tagRand]);
                } else {
                    continue;
                }
            }
            $article->setDate(new \DateTime($faker->date('d-m-Y', 'now')));
            $article->setText($faker->realText(1500));
            $manager->persist($article);
        }
        $manager->flush();
    }
}
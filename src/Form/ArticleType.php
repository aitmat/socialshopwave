<?php
namespace App\Form;

use App\Entity\Article;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('subject', TextType::class, [
                'label' => 'Тема',
                'required' => true,
            ])
            ->add('date', DateType::class, [
                'label' => 'Дата',
                'widget' => 'single_text',
                'required' => true,
                'by_reference' => true,
                'attr' => ['class' => 'js-datepicker'],
            ])
            ->add('tagsAsString', TextType::class, [
                'label' => 'Теги',
                'required' => true,
                'auto_initialize' => false,
                'attr' => [
                    'placeholder' => 'тег1, тег2, тег3 ...',
                    'autocomplete' => 'off',
                    ]
            ])
            ->add('text', TextareaType::class, [
                'label' => 'Текст',
                'required' => true,
                'attr' => ['class' => 'article-text-area'],
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Добавить статью',
                'attr' => ['class' => 'btn btn-primary']
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Article::class,
            'attr' => ['id' => 'myForm'],
            'required' => true,
            'validation_groups'
        ]);
    }
}
<?php

namespace App\Repository;

use App\Entity\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Article::class);
    }

    /**
     * @param $tag
     * @return Article[] Returns an array of Division objects
     */
    public function findAllByTag($tag)
    {
        return $this->createQueryBuilder('d')
            ->innerJoin('d.tags', 't')
            ->where('t.id = :tag')
            ->setParameter('tag', $tag)
            ->orderBy('d.date', 'DESC')
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @param $page
     * @param $limit
     * @return Article[] Returns an array of Division objects
     */
    public function findArticles($page = 1, $limit = 5)
    {
        return $this->createQueryBuilder('d')
            ->setMaxResults($limit)
            ->setFirstResult(( $limit * $page ) - $limit )
            ->orderBy('d.date', 'DESC')
            ->getQuery()
            ->getResult()
            ;
    }
}
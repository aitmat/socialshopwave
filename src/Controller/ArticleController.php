<?php


namespace App\Controller;

use App\Entity\Article;
use App\Entity\Tag;
use App\Form\ArticleType;
use App\Repository\ArticleRepository;
use App\Repository\TagRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ArticleController extends Controller
{
    public const ALL_DATA = 1;
    public const DATA_BY_TAGS = 2;

    /**
     * @Route("/", name="index")
     * @param ArticleRepository $repository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(ArticleRepository $repository)
    {
        $articles = $repository->findArticles();
        return $this->render('articles.html.twig', [
            'articles' => $articles,
            'title' => 'Блог',
            'data_from' => self::ALL_DATA
        ]);
    }

    /**
     * @Route("/showe_article/{id}", name="show_article")
     * @param ArticleRepository $repository
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(ArticleRepository $repository, $id)
    {
        $article = $repository->find($id);
        return $this->render('show_article.html.twig', [
            'article' => $article
        ]);
    }

    /**
     * @Route("/tag/articles/{id}", name="show_tag_article")
     * @param ArticleRepository $repository
     * @param TagRepository $tagRepository
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showTagArticlesAction(ArticleRepository $repository, TagRepository $tagRepository, $id)
    {
        $articles = $repository->findAllByTag($id);
        $tag = $tagRepository->find($id);
        return $this->render('articles.html.twig', [
            'articles' => $articles,
            'title' => $tag->getTag(),
            'data_from' => self::DATA_BY_TAGS
        ]);
    }

    /**
     * @Route("/create", name="create_article")
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @param TagRepository $repository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createArticleAction(Request $request, EntityManagerInterface $manager, TagRepository $repository)
    {
        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Article $formData */
            $formData = $form->getData();
            $tagsString = $formData->getTagsAsString();
            $arrayTags = explode(',', $tagsString);
            array_walk($arrayTags, function (&$value){
                    $value = trim($value);
            });
            $arrayTags = array_unique($arrayTags);
            foreach ($arrayTags as $tag){
                $tag = trim($tag);
                if ($tag == ''){
                    continue;
                }
                if ($issetTag = $repository->findByTagName($tag)){
                    $formData->addTag($issetTag);
                } else{
                    $newTag = new Tag();
                    $newTag->setTag($tag);
                    $formData->getTags()->add($newTag);
                }
            }
            $manager->persist($formData);
            $manager->flush();
            $this->addFlash('notice', 'Операция успешно выполнена!');
            return $this->redirectToRoute('index');
        }
        $allTags = $repository->findAll();
        return $this->render('create_article.html.twig', [
            'form' => $form->createView(),
            'tags' => $allTags,
        ]);
    }

    /**
     * @param Request $request
     * @param ArticleRepository $articleRepository
     * @param \Twig_Environment $twig
     * @return Response
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     * @Route("page-api", name="article_api", defaults={"_format"="json"})
     */
    public function paginationApi(Request $request, ArticleRepository $articleRepository, \Twig_Environment $twig ){
        $limit = 5;
        $pager = $request->query->get('page') ? $request->query->get('page') : 1;
        $pages = $articleRepository->findArticles($pager, $limit);
        $result = [];
        $response = [
              'messages' => [
                  'success' => [],
                  'warning' => []
              ],
            'result' => '',
            'empty' => null,
        ];
        if ($pages){
            foreach ($pages as $page){
                $result[] = $twig->render('article_card.html.twig', [
                    'article' => $page
                ]);
            }
            $response['result'] = $result;

        } else {
            $response['empty'] = 1;
        }
        $jsonResponse = new JsonResponse();
        $jsonResponse->headers->set('Content-Type', 'application/json');
        $jsonResponse->setEncodingOptions(JSON_UNESCAPED_UNICODE);
        $jsonResponse->setData($response);
        return $jsonResponse;
    }

    public function recentArticleTags(TagRepository $repository)
    {
        $tags = $repository->findAll();
        return $this->render('tag_list.html.twig', ['tags' => $tags]);
    }
}